<?php

namespace Dropkick\Core\Metadata;

use Dropkick\Core\Formattable\FormattableString;

/**
 * Class Factory.
 *
 * A generic implementation of FactoryInterface.
 */
class Factory implements FactoryInterface {

  /**
   * The key to callable mapping.
   *
   * @var callable[]
   */
  protected $mapping = [];

  /**
   * {@inheritdoc}
   */
  public function hasMapping($key) {
    // Registered mapping handler.
    if (isset($this->mapping[$key])) {
      return TRUE;
    }

    // Otherwise indicates a specific class.
    return class_exists($key) && in_array(MetadataInterface::class, class_implements($key));
  }

  /**
   * {@inheritdoc}
   */
  public function getMetadata($class, $key, array $metadata) {
    // Confirm that we can generate the metadata for the key.
    if (!$this->hasMapping($key)) {
      throw new FactoryException(
        FormattableString::create(
          "{{ key }}} is unsupported for metadata generation.",
          ['key' => $key]
        )
      );
    }

    // Use the metadata generator.
    if (isset($this->mapping[$key])) {
      $object = $this->mapping[$key]($class, $metadata);
      if ($object instanceof MetadataInterface) {
        return $object;
      }
      throw new FactoryException(
        FormattableString::create(
          "{{ key }} generated a non-metadata result.",
          ['key' => $key]
        )
      );
    }

    // Generate from the class name.
    return new $key($class, $metadata);
  }

  /**
   * {@inheritdoc}
   */
  public function register($key, callable $callable) {
    $this->mapping[$key] = $callable;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function unregister($key) {
    unset($this->mapping[$key]);
    return $this;
  }

}
