<?php

namespace Dropkick\Core\Metadata;

use PhpParser\NodeTraverser;
use PhpParser\ParserFactory;

/**
 * Class Discovery.
 *
 * A generic implementation of the discovery process that processes PHP files
 * using a class constant for the metadata.
 */
class Discovery implements DiscoveryInterface {

  /**
   * {@inheritdoc}
   */
  public function canProcess($filename) {
    return pathinfo($filename, PATHINFO_EXTENSION) === 'php';
  }

  /**
   * {@inheritdoc}
   */
  public function getMetadata($filename) {
    // Create the parser for the system.
    $parser = (new ParserFactory())->create(ParserFactory::PREFER_PHP7);

    // Create the AST for the filename.
    try {
      $ast = $parser->parse((string) file_get_contents($filename));
    }
    catch (\Exception $e) {
      return [];
    }

    // Create visitor to extract information.
    $visitor = new DiscoveryVisitor();

    // Create node traversal class.
    $traveller = new NodeTraverser();
    $traveller->addVisitor($visitor);

    // Traverse the statements, extracting information.
    $traveller->traverse($ast);

    // Return the accumulated metadata.
    return $visitor->getMetadata();
  }

}
