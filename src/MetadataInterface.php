<?php

namespace Dropkick\Core\Metadata;

/**
 * Interface MetadataInterface.
 *
 * Provides the basic interface for processing metadata for an object.
 */
interface MetadataInterface {

  /**
   * Get the identifier for the metadata.
   *
   * @return string
   *   The identifier.
   */
  public function id();

  /**
   * Get the label for the metadata.
   *
   * @return \Dropkick\Core\Formattable\FormattableInterface
   *   The human readable label.
   */
  public function label();

  /**
   * Confirm the metadata has a key.
   *
   * @param string $key
   *   The key to check for metadata.
   *
   * @return bool
   *   Confirms the key exists for the metadata.
   */
  public function has($key);

  /**
   * Get the metadata by key, with default fallback.
   *
   * @param string $key
   *   The key to get the metadata.
   * @param mixed $default
   *   The default value for the metadata if not already provided.
   *
   * @return mixed
   *   The value.
   */
  public function get($key, $default = NULL);

  /**
   * Get the fully qualified class name for this metadata.
   *
   * @return string
   *   The class used for the metadata.
   */
  public function getClass();

  /**
   * Get defaults that apply to the initial values.
   *
   * @return array
   *   The defaults.
   */
  public function getDefaults();

}
