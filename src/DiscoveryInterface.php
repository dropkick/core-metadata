<?php

namespace Dropkick\Core\Metadata;

/**
 * Interface DiscoveryInterface.
 *
 * The core of the metadata is the discovery when processing files.
 */
interface DiscoveryInterface {

  /**
   * Confirms that the filename can be processed.
   *
   * @param string $filename
   *   The filename that possibly contains metadata.
   *
   * @return bool
   *   Confirmation the file can be processed.
   */
  public function canProcess($filename);

  /**
   * Extracts the metadata from the filename.
   *
   * @param string $filename
   *   The filename that possibly contains metadata.
   *
   * @return array[]
   *   The metadata as processed from the file.
   */
  public function getMetadata($filename);

}
