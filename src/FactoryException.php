<?php

namespace Dropkick\Core\Metadata;

/**
 * Class FactoryException.
 *
 * Triggered when any factory error occurs.
 */
class FactoryException extends \Exception {

}
