<?php

namespace Dropkick\Core\Metadata;

use PhpParser\Node\Name;
use PhpParser\Node\Identifier;
use PhpParser\Node\Expr\ClassConstFetch;
use PhpParser\Node\Expr;
use PhpParser\Node\Stmt\ClassConst;
use PhpParser\Node\Stmt\Use_;
use PhpParser\Node\Stmt\Class_;
use PhpParser\Node\Stmt\Namespace_;
use Dropkick\Core\Formattable\FormattableString;
use PhpParser\ConstExprEvaluationException;
use PhpParser\ConstExprEvaluator;
use PhpParser\Node;
use PhpParser\NodeVisitorAbstract;

/**
 * Class DiscoveryVisitor.
 *
 * A visitor that processes the AST to extract the constant value of a defined
 * object. The constant is usually METADATA.
 */
class DiscoveryVisitor extends NodeVisitorAbstract {

  /**
   * The namespace for the class where the metadata is coming.
   *
   * @var string
   */
  protected $namespace;

  /**
   * The class name for which the constant is expected.
   *
   * @var string
   */
  protected $className;

  /**
   * Provides the uses mapping.
   *
   * @var string[]
   */
  protected $uses = [];

  /**
   * A list of all the classes and their metadata.
   *
   * @var array
   */
  protected $const = [];

  /**
   * The metadata loaded from the class.
   *
   * @var array
   */
  protected $metadata = [];

  /**
   * Get the metadata extracted from the AST.
   *
   * @return array
   *   The metadata.
   */
  public function getMetadata() {
    return $this->const;
  }

  /**
   * {@inheritdoc}
   */
  public function enterNode(Node $node) {
    // Keep record of the namespace.
    if ($node instanceof Namespace_) {
      $this->namespace = (string) $node->name;
      return NULL;
    }
    // Keep a record of the class.
    if ($node instanceof Class_) {
      $this->className = (string) $node->name;
      return NULL;
    }
    if ($node instanceof Use_) {
      // Cycle through all the uses statements, providing a mapping for later
      // evaluation.
      foreach ($node->uses as $use) {
        $this->uses[(string) $use->getAlias()] = (string) $use->name;
      }
      return NULL;
    }

    // Process any class constants for METADATA.
    if ($node instanceof ClassConst) {
      $class_name = ($this->namespace ? $this->namespace . '\\' : '') . $this->className;
      foreach ($node->consts as $const) {
        // We must match METADATA to detect the values.
        if ((string) $const->name !== 'METADATA') {
          continue;
        }

        // Evaluate a strict constant value.
        $evaluator = new ConstExprEvaluator([$this, 'evaluate']);
        $this->const[$class_name] = $evaluator->evaluateDirectly($const->value);
      }

      return NULL;
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function leaveNode(Node $node) {
    if ($node instanceof Namespace_) {
      unset($this->namespace);
      return NULL;
    }
    if ($node instanceof Class_) {
      unset($this->className);
      return NULL;
    }
    return NULL;
  }

  /**
   * Allow evaluation of expressions which are not simple.
   *
   * @param \PhpParser\Node\Expr $expr
   *   The AST expression.
   *
   * @return string
   *   Converted class constant name.
   *
   * @throws \PhpParser\ConstExprEvaluationException
   *   Triggered when an unsupported expression is being evaluated.
   */
  public function evaluate(Expr $expr) {
    // We can only convert class names from uses statements.
    if ($expr instanceof ClassConstFetch &&
        $expr->name instanceof Identifier &&
        $expr->name->toString() === 'class') {
      $class = $expr->class instanceof Name ? $expr->class->toString() : 'invalid-expression';
      if (array_key_exists($class, $this->uses)) {
        return $this->uses[$class];
      }
    }

    throw new ConstExprEvaluationException(
      FormattableString::create(
        "Expression of type {{ type }} cannot be evaluated",
        ['type' => $expr->getType()]
      )
    );
  }

}
