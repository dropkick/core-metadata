<?php

namespace Dropkick\Core\Metadata;

/**
 * Interface FactoryInterface.
 *
 * Provides a factory interface for mapping metadata to objects.
 */
interface FactoryInterface {

  /**
   * Confirm that the key has an mapping mechanism.
   *
   * @param string $key
   *   The key to check for a mapping.
   *
   * @return bool
   *   Confirmation of existence.
   */
  public function hasMapping($key);

  /**
   * Converts the metadata into the appropriate metadata object.
   *
   * @param string $class
   *   The class which has the metadata.
   * @param string $key
   *   The key to generate the metadata.
   * @param array $metadata
   *   The metadata as an array.
   *
   * @return \Dropkick\Core\Metadata\MetadataInterface
   *   A metadata instance.
   *
   * @throws \Dropkick\Core\Metadata\FactoryException
   *   Triggered when the metadata does not successfully initialize.
   */
  public function getMetadata($class, $key, array $metadata);

  /**
   * Register a mapping generator.
   *
   * @param string $key
   *   The key to map.
   * @param callable $callable
   *   The callable used to generate the metadata.
   *
   * @return static
   *   The factory object.
   */
  public function register($key, callable $callable);

  /**
   * Unregisters a mapped generator.
   *
   * @param string $key
   *   The key to unmap.
   *
   * @return static
   *   The factory object.
   */
  public function unregister($key);

}
