<?php

namespace Dropkick\Core\Metadata;

/**
 * Interface EditableMetadataInterface.
 *
 * Allow the normally immutable metadata to be editable.
 */
interface EditableMetadataInterface extends MetadataInterface {

  /**
   * Set the value for the key.
   *
   * @param string $key
   *   The key to be set with value.
   * @param mixed $value
   *   Setting the value to NULL should remove the key.
   *
   * @return static
   *   The metadata object.
   */
  public function set($key, $value);

}
