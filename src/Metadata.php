<?php

namespace Dropkick\Core\Metadata;

use Dropkick\Core\Formattable\FormattableString;

/**
 * Class Metadata.
 *
 * A generic implementation of MetadataInterface.
 */
class Metadata implements MetadataInterface {

  /**
   * The class described by the metadata.
   *
   * @var string
   */
  protected $class;

  /**
   * The metadata information used internally.
   *
   * @var array
   */
  protected $metadata;

  /**
   * Metadata constructor.
   *
   * @param string $class
   *   The class using the metadata.
   * @param array $metadata
   *   The stored value of the metadata.
   */
  public function __construct($class, array $metadata) {
    $this->class = $class;
    $this->metadata = $metadata + $this->getDefaults();
  }

  /**
   * {@inheritdoc}
   */
  public function id() {
    return $this->get('id');
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    return FormattableString::create($this->get('label', $this->id()));
  }

  /**
   * {@inheritdoc}
   */
  public function has($key) {
    return array_key_exists($key, $this->metadata);
  }

  /**
   * {@inheritdoc}
   */
  public function get($key, $default = NULL) {
    if (array_key_exists($key, $this->metadata)) {
      return $this->metadata[$key];
    }
    return $default;
  }

  /**
   * {@inheritdoc}
   */
  public function getClass() {
    return $this->class;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaults() {
    return [];
  }

}
