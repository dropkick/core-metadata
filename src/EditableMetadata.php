<?php

namespace Dropkick\Core\Metadata;

/**
 * Class EditableMetadata.
 *
 * A generic example of EditableMetadataInterface.
 */
class EditableMetadata extends Metadata implements EditableMetadataInterface {

  /**
   * {@inheritdoc}
   */
  public function set($key, $value) {
    $this->metadata[$key] = $value;
    return $this;
  }

}
