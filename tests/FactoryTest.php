<?php

namespace Dropkick\Core\Metadata;

use PHPUnit\Framework\TestCase;

class FactoryTest extends TestCase {


  /**
   * @var \Dropkick\Core\Metadata\Factory
   */
  protected $factory;

  public function setUp(): void {
    $this->factory = new Factory();
  }

  public function testHasMapping() {
    $factory = $this->factory;

    $this->assertFalse($factory->hasMapping('one'));
    $this->assertTrue($factory->hasMapping(Metadata::class));
    $this->assertFalse($factory->hasMapping(self::class));
  }

  public function testRegister() {
    $factory = $this->factory;

    // Register and then unregister
    $factory->register('one', [$this, 'getMetadata']);
    $this->assertTrue($factory->hasMapping('one'));

    $data = ['one' => TRUE];
    $metadata = $factory->getMetadata(self::class, 'one', $data);
    $this->assertNotEmpty($metadata);
    $this->assertEquals(Metadata::class, get_class($metadata));
    $this->assertEquals(self::class, $metadata->getClass());
    $this->assertEquals(TRUE, $metadata->get('one'));

    $factory->unregister('one');
    $this->assertFalse($factory->hasMapping('one'));
  }

  public function testGetMetadataNotExist() {
    $this->expectException(FactoryException::class);
    $this->factory->getMetadata(self::class, 'one', []);
  }

  public function testGetMetadataWrongObject() {
    $this->expectException(FactoryException::class);
    $this->factory->register('one', function () {
      return new \stdClass();
    });
    $this->factory->getMetadata(self::class, 'one', []);
  }

  public function testGetMetadata() {
    $metadata = $this->factory->getMetadata(self::class, Metadata::class, ['one' => TRUE]);
    $this->assertNotEmpty($metadata);
    $this->assertEquals(Metadata::class, get_class($metadata));
    $this->assertEquals(self::class, $metadata->getClass());
    $this->assertEquals(TRUE, $metadata->get('one'));
  }

  public function getMetadata($class, $metadata) {
    return new Metadata($class, $metadata);
  }
}
