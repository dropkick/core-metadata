<?php

namespace Dropkick\Core\Metadata;

use Dropkick\Core\Metadata\fixtures\ClassMetadata;
use Dropkick\Core\Metadata\fixtures\MultipleMetadata;
use Dropkick\Core\Metadata\fixtures\OneMetadata;
use PhpParser\ConstExprEvaluationException;
use PHPUnit\Framework\TestCase;

class DiscoveryTest extends TestCase {

  /**
   * @var \Dropkick\Core\Metadata\Discovery
   */
  protected $discovery;

  public function setUp(): void {
    $this->discovery = new Discovery();
  }

  public function testWrongFile() {
    $this->assertEquals(FALSE, $this->discovery->canProcess(__DIR__ . '/OneMetadata.yml'));
    $this->assertEquals(TRUE, $this->discovery->canProcess(__DIR__ . '/OneMetadata.php'));
  }

  public function testParseFail() {
    $metadata = $this->discovery->getMetadata(__DIR__ . '/fixtures/ParseFail.php');
    $this->assertEmpty($metadata);
    $this->assertTrue(is_array($metadata));
  }

  public function testNoMetadata() {
    $metadata = $this->discovery->getMetadata(__DIR__ . '/fixtures/NoMetadata.php');
    $this->assertEmpty($metadata);
    $this->assertTrue(is_array($metadata));
  }

  public function testOneMetadata() {
    $metadata = $this->discovery->getMetadata(__DIR__ . '/fixtures/OneMetadata.php');
    $this->assertTrue(is_array($metadata));
    $this->assertEquals(count($metadata), 1);
    $this->assertArrayHasKey(OneMetadata::class, $metadata);

    $metadata = $metadata[OneMetadata::class];
    $this->assertEquals(count($metadata), 1);
    $this->assertArrayHasKey('one', $metadata);
  }

  public function testMultipleMetadata() {
    $metadata = $this->discovery->getMetadata(__DIR__ . '/fixtures/MultipleMetadata.php');
    $this->assertTrue(is_array($metadata));
    $this->assertEquals(count($metadata), 1);
    $this->assertArrayHasKey(MultipleMetadata::class, $metadata);

    $metadata = $metadata[MultipleMetadata::class];
    $this->assertEquals(count($metadata), 2);
    $this->assertArrayHasKey('one', $metadata);
    $this->assertArrayHasKey('two', $metadata);
  }

  public function testClassMetadata() {
    $metadata = $this->discovery->getMetadata(__DIR__ . '/fixtures/ClassMetadata.php');
    $this->assertTrue(is_array($metadata));
    $this->assertEquals(count($metadata), 1);
    $this->assertArrayHasKey(ClassMetadata::class, $metadata);

    $metadata = $metadata[ClassMetadata::class];
    $this->assertEquals(count($metadata), 1);
    $this->assertArrayHasKey(Metadata::class, $metadata);
  }

  public function testUndefinedClassMetadata() {
    $this->expectException(ConstExprEvaluationException::class);
    $metadata = $this->discovery->getMetadata(__DIR__ . '/fixtures/UndefinedClassMetadata.php');
  }
}
