<?php

namespace Dropkick\Core\Metadata\fixtures;


class MultipleMetadata {

  const PRIOR = 'true';

  const NO_METADATA = TRUE;

  const METADATA = [
    'one' => [
      'value' => 'one',
    ],
    'two' => [
      'value' => 'one',
    ],
  ];

}