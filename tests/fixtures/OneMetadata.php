<?php

namespace Dropkick\Core\Metadata\fixtures;


class OneMetadata {

  const METADATA = [
    'one' => [
      'value' => 'one',
    ],
  ];

}