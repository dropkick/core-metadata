<?php

namespace Dropkick\Core\Metadata\fixtures;

use Dropkick\Core\Metadata\Metadata;

class ClassMetadata {

  const METADATA = [
    Metadata::class => [
      'value' => 'one',
    ],
  ];

}
