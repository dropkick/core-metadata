<?php

namespace Dropkick\Core\Metadata\fixtures;

class UndefinedClassMetadata {

  const METADATA = [
    Metadata::class => [
      'value' => 'one',
    ],
  ];

}
