<?php

namespace Dropkick\Core\Metadata;

use PHPUnit\Framework\TestCase;

class EditableMetadataTest extends TestCase {

  /**
   * @var \Dropkick\Core\Metadata\EditableMetadata
   */
  protected $metadata;

  public function setUp(): void {
    $this->metadata = new EditableMetadata(self::class, [
      'id' => 'test_id',
      'label' => 'Test ID',
      'one' => TRUE
    ]);
  }

  public function testSetGet() {
    $metadata = $this->metadata;

    $this->assertEquals('test_id', $metadata->id());
    $this->assertEquals('Test ID', (string)$metadata->label());
    $this->assertEquals(self::class, $metadata->getClass());
    $this->assertFalse($metadata->has('two'));
    $this->assertTrue($metadata->has('one'));
    $this->assertEquals(TRUE, $metadata->get('one'));

    $metadata->set('two', self::class);
    $this->assertTrue($metadata->has('two'));
    $this->assertEquals(self::class, $metadata->get('two'));

    $this->assertEquals(self::class, $metadata->get('undefined', self::class));
  }
}
